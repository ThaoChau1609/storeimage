var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require("path");
router.use(express.static(path.join(__dirname, 'public')));
var config = fs.readFileSync('./app_config.json', 'utf8');
config = JSON.parse(config);
const fileUpload = require('express-fileupload');
request = require('request');

var AWS = require("aws-sdk");
AWS.config.region = config.AWS_REGION;
AWS.config.accessKeyId = config.accessKey_Id;
AWS.config.secretAccessKey = config.secret_AccessKey;

var s3 = new AWS.S3();
var db = new AWS.DynamoDB();
var myBucket = config.Bucket_Name;
module.exports = router;

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Express' });
});

router.post('/CheckLogin', async (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    var params = {
        TableName: 'StoreImageAccount',
        Key: {
            'UserName': { S: username }
        },
        AttributesToGet: [
            "UserName",
            "Password"
        ]
    };

    await db.getItem(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            if (data.Item != null) {
                console.log("Success", data.Item);

                console.log(JSON.parse(JSON.stringify(data.Item)).Password.S);
                if (JSON.parse(JSON.stringify(data.Item)).Password.S == password) {
                    console.log("OK");
                    res.redirect('/Home');
                }
                else {
                    console.log("Not ok");
                    res.redirect('/');
                }
            }
            else {
                console.log("Item not found");
                res.redirect('/');
            }
        }
    });
});



router.post('/SignUp', function (req, res) {
    var user = req.body.user;
    var password = req.body.psw;
    var params = {
        RequestItems: {
            "StoreImageAccount": [
                {
                    PutRequest: {
                        Item: {
                            "UserName": { S: user },
                            "Password": { S: password }
                        }
                    }
                }
            ]
        }
    };

    db.batchWriteItem(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            console.log("Success", data);
        }
    });

    res.redirect("/");
});

router.get('/Home', async (req, res) => {
    await fs.readFile('public/layout.html', async (err, data) => {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data);
        res.end();
    });
});

router.post('/Upload', async function (req, res) {
    if (Object.keys(req.files).length == 0) {
        return res.status(400).send('No files were uploaded.');
    }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let myFile = req.files.myFile;

    myKey = myFile.name;
    params =
        {
            Bucket: myBucket,
            Key: myKey,
            Body: myFile.data,
            ACL: 'public-read',
            ContentType: 'image'
        };

    await s3.putObject(params, function (err, data) {

        if (err) {

            console.log(err)
        } else {

            console.log("Successfully uploaded " + data);
            var pr = {
                Bucket: myBucket,
                Key: myKey
            };

            res.redirect("/Home"); // reload Page
        }
    });

});

var text = "";
router.get('/getAll', async function (req, res) {
    s3.listObjectsV2({ Bucket: myBucket }, async function (err, data) {


        if (err) console.log(err, err.stack); // an error occurred
        else {
            text = "";
            for (var i = 0; i < data.Contents.length; i++) {
                var imgID = data.Contents[i].Key;
                var param = {
                    Bucket: myBucket,
                    Key: imgID
                };
                var url = await s3.getSignedUrl('getObject', param);
                //?txtYear=" +data.Contents[i].Key + "
                text += "<form id='frmDowload' method='post'  name='dowloadForm'" +
                    "                              encType='multipart/form-data'>" +
                    "      <div class='col-md-3' > " +
                    "        <div class='thumbnail'>" +
                    "            <a href='" + url + "' target='_blank'>" +
                    "               <img src='" + url + "' class='img' name='imgs' id='" + imgID + "'>" +
                    "            </a>" +
                    "            <input type='hidden' value='" + imgID + "' name='txtKey'>" +
                    "<input type='submit' name='button' value='Dowload' formaction='/Dowload' style='background-color: #008CBA; color: white;text-align: center; border-radius: 20%'/>" +
                    "<input type='submit' name='button' value='Delete' formaction='/Delete' style='margin-left: 20px; background-color: #008CBA; color: white;text-align: center;border-radius: 20%'/>" +
                    "       </div>" +
                    "      </div>" + "</form>";

            }

            res.write(text);
            res.end();
        }
        /* console.log(map.get());*/
    });
});

router.get('/Search/:name', async function (req, res) {

    s3.listObjectsV2({ Bucket: myBucket }, async function (err, data) {


        if (err) console.log(err, err.stack); // an error occurred
        else {
            text = "";
            for (var i = 0; i < data.Contents.length; i++) {
                var imgID = data.Contents[i].Key;
                if (imgID.startsWith(req.params.name)) {
                    var param = {
                        Bucket: myBucket,
                        Key: imgID
                    };
                    var url = await s3.getSignedUrl('getObject', param);
                    //?txtYear=" +data.Contents[i].Key + "
                    text += "<form id='frmDowload' method='post'  name='dowloadForm'" +
                        "                              encType='multipart/form-data'>" +
                        "      <div class='col-md-3'>" +
                        "        <div class='thumbnail'>" +
                        "            <a href='" + url + "' target='_blank'>" +
                        "               <img src='" + url + "' class='img' name='imgs' id='" + imgID + "'>" +
                        "            </a>" +
                        "            <input type='hidden' value='" + imgID + "' name='txtKey'>" + 
                        "<input type='submit' name='button' value='Dowload' formaction='/Dowload' style='background-color: #008CBA; color: white;text-align: center; border-radius: 20%'/>" +
                        "<input type='submit' name='button' value='Delete' formaction='/Delete' style='margin-left: 20px; background-color: #008CBA; color: white;text-align: center;border-radius: 20%'/>" +
                        "       </div>" +
                        "      </div>" + "</form>";

                }
            }
            if (text == "") {
                text = "<div>Cannot found image start with \"" + req.params.name + "\"</div>"
            }
            res.write(text);
            res.end();
        }
        /* console.log(map.get());*/
    });
});

router.post('/Dowload', async function (req, respone) {

    var imgID = req.body.txtKey;
    console.log(imgID);
    var download = function (uri, filename, callback) {
        request.head(uri, async (err, res, body) => {
            console.log('content-type:', res.headers['content-type']);
            await request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
            await respone.download(filename, filename);
        });
    };

    // var s3 = new AWS.S3();
    var s3Params = {
        Bucket: 'picturestore97',
        Key: imgID
    };

    const url = await s3.getSignedUrl('getObject', s3Params);
    console.log(url);


    await download(url, imgID, function () {
        console.log('done' + url);
    });
});

router.post("/Delete", async function (req, res) {
    var imgID = req.body.txtKey;
    console.log(imgID);
    var params = {
        Bucket: 'picturestore97',
        Key: imgID
    };
    await s3.deleteObject(params, function (err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else {
            console.log("Delete success image: " + imgID);  // successful response
            res.redirect("/Home"); // reload Page
        }
    });
});


