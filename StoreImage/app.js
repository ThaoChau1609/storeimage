var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require("body-parser");
var fileupload = require("express-fileupload");
var logger = require('morgan');
var routes = require('./routes');
var http = require('http');
var indexRouter = require('./routes/index');

var app = express();


app.set('port', process.env.PORT || 8080);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(fileupload());
app.use('/', indexRouter);
app.use(bodyParser.json({limit: '150mb'}));
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    limit: '150mb',
    extended: true
}));
//module.exports = app;
app.get('/', indexRouter);
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

